# guix-tools #

guix-tools is a collection of tools to develop GNU Guix.

## License ##

guix-tools are copyrighted 2018 by Björn Höfling.
guix-tools are licensed under the GNU GPL. Either version 3 or any later
version, at your option.
